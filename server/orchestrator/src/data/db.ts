import { config } from '@orchestrator/config';
import knex from 'knex';

export const dbClient = knex({
  client: 'postgres',
  connection: {
    host: config.db.host,
    port: config.db.port,
    user: config.db.user,
    database: config.db.name,
    password: config.db.password,
    typeCast: (field: any, next: any) => {
      if (field.type === 'JSON') {
        return JSON.parse(field.string());
      }

      return next();
    },
    ssl: true,
  },
});
