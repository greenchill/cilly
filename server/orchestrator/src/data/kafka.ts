import { SchemaRegistry, SchemaType } from '@kafkajs/confluent-schema-registry';
import { DateLogicalType, BigIntLogicalType } from '@common/kafka/logicalTypes';
import { config } from '@orchestrator/config';
import { Kafka } from 'kafkajs';

export const kafka = new Kafka({
  clientId: 'cilly-orchestrator',
  brokers: config.kafka.brokers,
});

export const kafkaSchemaRegistry = new SchemaRegistry(
  { host: config.kafka.schemaRegistry.host },
  {
    [SchemaType.AVRO]: {
      logicalTypes: {
        'timestamp-millis': DateLogicalType,
        bigint: BigIntLogicalType,
      },
    },
  },
);

export async function loadKafkaSchemas() {
  // TODO
}
