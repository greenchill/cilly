import { coreConfig } from "@common/config";
import { consumerConfig } from "@orchestrator/config/consumer";

export const config = {
  ...coreConfig,
  consumer: consumerConfig,
}
