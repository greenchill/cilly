import { mustExist } from "@common/utils/exists";

export const consumerConfig = {
  controller: {
    maxPartitionsAtOnce: parseInt(mustExist(process.env.CONTROLLER_MAX_PARTITIONS_AT_ONCE), 10),
  }
};
