import { Elysia } from 'elysia';
import { logger } from '@common/logging/logger';

const app = new Elysia().get('/', () => 'Hello Elysia').listen(3000);

logger.info(`Server is running at ${app.server?.hostname}:${app.server?.port}`);
