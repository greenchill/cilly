import { KafkaEvent } from '@common/events/decorator';
import { SerializableEvent } from '@common/events/serializable';
import { GraphNodeStatusKind } from '@orchestrator/graph/state';

export type GraphEventData = {
  graphId: string;
  runId: string;
};

export type GraphNodeEventData = GraphEventData & {
  nodeId: string;
  status: GraphNodeStatusKind;
};

export type GraphNodeStartedEventData = GraphNodeEventData & {
  startedAt: number;
};

export type GraphNodeFinishedEventData = GraphNodeEventData & {
  finishedAt: number;
  success: boolean;
};

export type GraphNodePausedEventData = GraphNodeEventData & {
  pausedAt: number;
};

export type GraphNodeResumedEventData = GraphNodeEventData & {
  resumedAt: number;
};

@KafkaEvent('graph-state.node-started')
export class GraphNodeStartedEvent extends SerializableEvent<GraphNodeStartedEventData> {}

@KafkaEvent('graph-state.node-finished')
export class GraphNodeFinishedEvent extends SerializableEvent<GraphNodeFinishedEventData> {}

@KafkaEvent('graph-state.node-paused')
export class GraphNodePausedEvent extends SerializableEvent<GraphNodeStartedEventData> {}

@KafkaEvent('graph-state.node-resumed')
export class GraphNodeResumedEvent extends SerializableEvent<GraphNodeResumedEventData> {}

export type GraphNodeEvent =
  | GraphNodeStartedEvent
  | GraphNodeFinishedEvent
  | GraphNodePausedEvent
  | GraphNodeResumedEvent;

export function isGraphNodeEvent(event: SerializableEvent) {
  if (event instanceof GraphNodeStartedEvent) {
    return true;
  }

  if (event instanceof GraphNodeFinishedEvent) {
    return true;
  }

  if (event instanceof GraphNodePausedEvent) {
    return true;
  }

  if (event instanceof GraphNodeResumedEvent) {
    return true;
  }

  return false;
}
