import { exists } from '@common/utils/exists';
import { GraphError } from '@orchestrator/graph/error';

export class GraphNode<TValue extends any = null> {
  public readonly inNeighbors = new Set<GraphNode<TValue>>();
  public readonly outNeighbors = new Set<GraphNode<TValue>>();
  private nodeValue: TValue | null = null;

  public get edges(): Set<GraphNode<TValue>> {
    return new Set([...this.inNeighbors, ...this.outNeighbors]);
  }

  public get value(): TValue | null {
    return this.nodeValue || null;
  }

  public constructor(public readonly id: string, value?: TValue) {
    this.nodeValue = value || null;
  }

  public addInEdge(node: GraphNode<TValue>) {
    this.inNeighbors.add(node);
  }

  public addOutEdge(node: GraphNode<TValue>) {
    this.outNeighbors.add(node);
  }

  public updateValue(value: TValue) {
    this.nodeValue = value;
  }
}

export class DirectedAcyclicGraph<TNodeValue extends any = null> {
  private nodes = new Map<string, GraphNode<TNodeValue>>();

  public get allNodes() {
    return Array.from(this.nodes.values());
  }

  public static fromGraph(graph: DirectedAcyclicGraph) {
    
  }

  public getNode(id: string): GraphNode<TNodeValue> {
    if (!this.nodes.has(id)) {
      throw new GraphError(`Node with id ${id} does not exist.`);
    }

    return this.nodes.get(id)!;
  }

  public addNode(id: string, value?: TNodeValue): GraphNode<TNodeValue> {
    if (this.nodes.has(id)) {
      throw new GraphError(`Node with id ${id} already exists.`);
    }

    const node = new GraphNode<TNodeValue>(id, value);
    this.nodes.set(id, node);
    return node;
  }

  public addEdge(fromId: string, toId: string): void {
    const fromNode = this.nodes.get(fromId);
    const toNode = this.nodes.get(toId);

    if (!exists(fromNode) || !exists(toNode)) {
      throw new GraphError('Either of both of the provided nodes do not exist.');
    }

    if (this.hasCycle(fromNode, toNode)) {
      throw new GraphError('Adding this edge would create a cycle.');
    }

    fromNode.addOutEdge(toNode);
    toNode.addInEdge(fromNode);
  }

  private hasCycle(fromNode: GraphNode<TNodeValue>, toNode: GraphNode<TNodeValue>): boolean {
    const visit = (node: GraphNode<TNodeValue>, visited: Set<GraphNode<TNodeValue>>): boolean => {
      if (node === toNode) {
        return true;
      }

      visited.add(node);
      for (const edge of node.edges) {
        if (!visited.has(edge) && visit(edge, visited)) {
          return true;
        }
      }

      return false;
    };

    return visit(fromNode, new Set());
  }
}
