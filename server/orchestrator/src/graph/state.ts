import { exists } from '@common/utils/exists';
import { DirectedAcyclicGraph } from '@orchestrator/graph/dag';
import { GraphStateError } from '@orchestrator/graph/error';
import { GraphNodeEvent, GraphNodeFinishedEvent, GraphNodePausedEvent, GraphNodeResumedEvent, GraphNodeStartedEvent, isGraphNodeEvent } from '@orchestrator/graph/event';

export const GraphStatus = {
  Pending: 'pending',
  Running: 'running',
  Succeeded: 'succeeded',
  Failed: 'failed',
} as const;

export const GraphNodeStatus = {
  Pending: 'pending',
  Running: 'running',
  Paused: 'paused',
  Succeeded: 'succeeded',
  Cancelled: 'cancelled',
  Failed: 'failed',
} as const;

export type GraphStatusKind = (typeof GraphStatus)[keyof typeof GraphStatus];
export type GraphNodeStatusKind = (typeof GraphNodeStatus)[keyof typeof GraphNodeStatus];

export class GraphState {
  private stateGraph = new DirectedAcyclicGraph<GraphNodeStatusKind>();
  private version = 0;

  public constructor(public readonly runId: string) {}

  public static fromEvents(events: Array<GraphNodeEvent>) {
    const state = new GraphState(events[0].data.runId);
    for (const event of events) {
      if (event.data.runId !== state.runId) {
        throw new GraphStateError('Not all provided events have the same runId', { expectedRunId: state.runId, gotRunId: event.data.runId });
      }

      state.applyEvent(event);
    }

    return state;
  }

  public getStatus() {
    let pendingCount = 0;
    for (const node of this.stateGraph.allNodes) {
      if (node.value === 'failed') {
        return GraphStatus.Failed;
      }

      if (node.value === 'running') {
        return GraphStatus.Running;
      }

      if (node.value === 'pending' || 'paused') {
        pendingCount++;
      }
    }

    return pendingCount === 0 ? GraphStatus.Succeeded : GraphStatus.Pending;
  }

  public canQueueOnNode(nodeId: string) {
    const node = this.stateGraph.getNode(nodeId);
    if (!exists(node)) {
      throw new GraphStateError(`Provided node with id ${nodeId} does not belong in the current graph`, { nodeId });
    }

    for (const inNeighbor of node.inNeighbors.values()) {
      if (inNeighbor.value === GraphNodeStatus.Cancelled) {
        continue;
      }

      if (inNeighbor.value === GraphNodeStatus.Succeeded) {
        continue;
      }

      return false;
    }

    return true;
  }

  // TODO CHECK CONDITIONS FOR UPDATES
  public applyEvent(event: GraphNodeEvent) {
    if (!isGraphNodeEvent(event)) {
      throw new GraphStateError(`Provided event with id ${event.id} is not a graph node event`, { event });
    }

    const node = this.stateGraph.getNode(event.data.nodeId);
    if (!exists(node)) {
      throw new GraphStateError(`Provided node with id ${event.data.nodeId} does not belong in the current graph`, { event });
    }

    if (event instanceof GraphNodeStartedEvent) {
      node.updateValue(GraphNodeStatus.Running);
    }

    if (event instanceof GraphNodeFinishedEvent) {
      node.updateValue(event.data.success ? GraphNodeStatus.Succeeded : GraphNodeStatus.Failed);
    }

    if (event instanceof GraphNodePausedEvent) {
      node.updateValue(GraphNodeStatus.Paused);
    }

    if (event instanceof GraphNodeResumedEvent) {
      node.updateValue(GraphNodeStatus.Running);
    }

    this.version++;
  }
}
