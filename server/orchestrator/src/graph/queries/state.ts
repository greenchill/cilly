import { getKafkaEventMetadata } from '@common/events/decorator';
import { createTransformStream } from '@common/kafka/ksql/stream';
import { GraphNodeEvent } from '@orchestrator/graph/event';
import { GraphNodeStatusKind } from '@orchestrator/graph/state';
import { ksqlClient } from '@orchestrator/services';

type SerializedStateEvent = {
  id: string;
  type: string;
  timestamp: number;
  data: {
    runId: string;
    graphId: string;
    nodeId: string;
    status: GraphNodeStatusKind;
    success?: boolean;
    startedAt?: string;
    finishedAt?: string;
    pausedAt?: string;
    resumedAt?: string;
  };
};

function deserializeStateEvent({
  id,
  type,
  timestamp,
  data,
}: SerializedStateEvent): GraphNodeEvent {
  const deserializedData = {
    runId: data.runId,
    nodeId: data.nodeId,
    graphId: data.graphId,
    status: data.status,
    startedAt: data.startedAt ? new Date(data.startedAt) : undefined,
    finishedAt: data.finishedAt ? new Date(data.finishedAt) : undefined,
    pausedAt: data.pausedAt ? new Date(data.pausedAt) : undefined,
    resumedAt: data.resumedAt ? new Date(data.resumedAt) : undefined,
  };

  const { constructor: EventConstructor } = getKafkaEventMetadata(type);
  const event = new EventConstructor(deserializedData, id, new Date(timestamp));
  return event as GraphNodeEvent;
}

export async function getGraphStateEventsByRunId(runId: string) {
  const responseStream = await ksqlClient.makeQuery<SerializedStateEvent>({
    properties: { runId },
    sql: `SELECT id, type, timestamp, data->graphId, \
            data->runId, data->nodeId, data->status, data->success, \
            data->startedAt, data->finishedAt, data->pausedAt, data->resumedAt
          FROM graph_node_updates
          WHERE data->runId = \${runId}`,
  });

  const transform = createTransformStream(deserializeStateEvent);
  responseStream.pipeThrough(transform);

  return transform.readable;
}
