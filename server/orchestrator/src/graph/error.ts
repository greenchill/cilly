import { DataError } from '@common/utils/error';
import { AnyObject } from '@common/utils/types';

export class GraphError extends DataError {
  public constructor(message?: string, data?: AnyObject) {
    super(message, data);
  }
}

export class GraphStateError extends DataError {
  public constructor(message?: string, data?: AnyObject) {
    super(message, data);
  }
}
