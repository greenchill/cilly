import { GraphController } from '@orchestrator/controller/controller';
import { consumerRegistry } from '@orchestrator/services';
import { EachMessagePayload } from 'kafkajs';
import { kafkaMessageToEvent } from '@common/events/map'; 
import { kafkaSchemaRegistry } from '@orchestrator/data/kafka';
import { GraphNodeEvent } from '@orchestrator/graph/event';
import { exists } from '@common/utils/exists';
import { KafkaGroup, KafkaTopic } from '@common/kafka/constants';

const controller = new GraphController();

const graphStateConsumer = consumerRegistry.getConsumer('graph-controller', {
  groupId: KafkaGroup.GraphState,
});

async function handleStateMessage({ message }: EachMessagePayload) {
  const event = await kafkaMessageToEvent<GraphNodeEvent>(message, kafkaSchemaRegistry);
  if (!exists(event)) {
    return;
  }

  controller.handleGraphUpdate(event);
}

export async function initController() {
  await graphStateConsumer.subscribe({ topics: [KafkaTopic.GraphState] });
  await graphStateConsumer.connect();

  await graphStateConsumer.run({
    eachMessage: handleStateMessage,
    partitionsConsumedConcurrently: 50,
  });
}
