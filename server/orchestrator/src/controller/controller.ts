import { streamIterator } from '@common/kafka/ksql/stream';
import { exists } from '@common/utils/exists';
import { GraphControllerError } from '@orchestrator/controller/error';
import { DirectedAcyclicGraph } from '@orchestrator/graph/dag';
import {
  GraphNodeEvent,
  GraphNodeFinishedEvent,
  isGraphNodeEvent,
} from '@orchestrator/graph/event';
import { getGraphStateEventsByRunId } from '@orchestrator/graph/queries/state';
import { GraphState } from '@orchestrator/graph/state';
import { producerRegistry } from '@orchestrator/services';
import { JobCreatedEvent } from '@common/job/event';

export class GraphController {
  private definitions: Record<string, DirectedAcyclicGraph> = {};
  private states: Record<string, GraphState> = {};

  public registerGraph(graphId: string, definition: DirectedAcyclicGraph) {
    if (exists(this.definitions[graphId])) {
      throw new GraphControllerError(`Graph with id ${graphId} already exists`);
    }

    this.definitions[graphId] = definition;
  }

  public registerGraphs(definitions: Array<[string, DirectedAcyclicGraph]>) {
    for (const [graphId, definition] of definitions) {
      this.registerGraph(graphId, definition);
    }
  }

  public startGraph(graphId: string) {
    const graph = this.definitions[graphId];
    if (!exists(graph)) {
      throw new GraphControllerError(`Graph with id ${graphId} does not exist`);
    }

    // TODO
  }

  public handleGraphUpdate(event: GraphNodeEvent) {
    if (!isGraphNodeEvent(event)) {
      throw new GraphControllerError(
        `Provided event with id ${event.id} is not a graph node event`,
      );
    }

    this.updateState(event);

    if (event instanceof GraphNodeFinishedEvent) {
      const definition = this.definitions[event.data.graphId];
      const state = this.states[event.data.runId];
      const nextNodes = definition.getNode(event.data.nodeId).outNeighbors;

      for (const node of nextNodes) {
        if (state.canQueueOnNode(node.id)) {
          this.queueJob();
        }
      }
    }
  }

  private async updateState(event: GraphNodeEvent) {
    if (!exists(this.states[event.data.runId])) {
      this.states[event.data.runId] = await this.loadGraphState(event.id, event.data.runId);
    }

    this.states[event.data.runId].applyEvent(event);
  }

  private async loadGraphState(eventId: string, runId: string): Promise<GraphState> {
    const state = new GraphState(runId);
    const eventStream = await getGraphStateEventsByRunId(runId);
    for await (const event of streamIterator(eventStream)) {
      if (event.id === eventId) {
        continue;
      }

      state.applyEvent(event);
    }

    return state;
  }

  private queueJob(runId: string, graphId: string, nodeId: string) {
    const producer = producerRegistry.getProducer('graph-controller');
    const jobEvent = new JobCreatedEvent({
      jobId: uuid.v4(),
      runId,
      graphId,
      nodeId,
    });

    producer.send({});
  }
}
