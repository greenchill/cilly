export class GraphControllerError extends Error {
  public constructor(message?: string) {
    super(message);
  }
}
