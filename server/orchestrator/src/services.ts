import { config } from '@orchestrator/config';
import { kafka } from '@orchestrator/data/kafka';
import { KsqlClient } from '@common/kafka/ksql/client';
import { ConsumerRegistry, ProducerRegistry } from '@common/kafka/registry';

export const consumerRegistry = new ConsumerRegistry(kafka);
export const producerRegistry = new ProducerRegistry(kafka);
export const ksqlClient = new KsqlClient(config.kafka.ksql.host);
