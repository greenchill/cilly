export default {
  development: {
    client: 'postgresql',
    migrations: {
      tableName: 'knex_migrations',
      directory: './migrations',
      loadExtensions: ['.ts'],
    },
    connection: {
      host: '127.0.0.1',
      port: 5432,
      database: 'cilly-orchestrator',
      user: 'postgres',
      password: 'postgres',
    },
  },
  production: {
    client: 'postgresql',
    migrations: {
      tableName: 'knex_migrations',
      directory: '/usr/src/app/migrations',
      loadExtensions: ['.ts'],
    },
    connection: {
      host: process.env.POSTGRES_HOST,
      port: parseInt(process.env.POSTGRES_PORT || '5432', 10),
      database: process.env.POSTGRES_DB,
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      ssl: {
        require: true,
        rejectUnauthorized: false,
      },
    },
    pool: {
      min: 2,
      max: 10,
    },
  },
};
