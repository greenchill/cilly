import { coreConfig } from '@common/config';
import { jsonFormat, prettyPrintFormat } from '@common/logging/format';
import { AnyObject } from '@common/utils/types';
import winston from 'winston';

export const logger: ILogger = winston.createLogger({
  level: coreConfig.isDevelopment ? 'debug' : 'info',
  transports: [
    new winston.transports.Console({
      format: coreConfig.isDevelopment ? prettyPrintFormat(true) : jsonFormat(),
    }),
  ],
});

interface ILogger {
  debug: (message: string, data?: AnyObject) => void;
  info: (message: string, data?: AnyObject) => void;
  warn: (message: string, data?: AnyObject) => void;
  error: (message: string, data?: AnyObject) => void;
}

class LoggerEnricher implements ILogger {
  public constructor(private readonly enrichWith: AnyObject) {}

  public debug(message: string, data?: AnyObject) {
    return logger.debug(message, { ...(data || {}), ...this.enrichWith });
  }

  public info(message: string, data?: AnyObject) {
    return logger.info(message, { ...(data || {}), ...this.enrichWith });
  }

  public warn(message: string, data?: AnyObject) {
    return logger.warn(message, { ...(data || {}), ...this.enrichWith });
  }

  public error(message: string, data?: AnyObject) {
    return logger.error(message, { ...(data || {}), ...this.enrichWith });
  }
}

export function enrichedLogger(enrichWith: AnyObject): ILogger {
  return new LoggerEnricher(enrichWith);
}
