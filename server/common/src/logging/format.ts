import { format } from 'winston';
import { isEmpty } from 'lodash';

const { json, printf, combine } = format;

export function jsonFormat() {
  return combine(
    format((info) => {
      info.level = info.level.toUpperCase();
      return info;
    })(),
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    json(),
    format.splat(),
  );
}

function developmentPrint(withData: boolean) {
  return printf(({ level, label, timestamp, message, ...data }) => {
    if (!withData || isEmpty(data)) {
      return `[${level}] [${timestamp}]: ${message}`;
    }

    const dataText = `\nData: ${JSON.stringify(data, (_, value) => {
      if (value?.type === 'Buffer') {
        return '[Buffer]';
      }

      if (Array.isArray(value) && value.length > 10) {
        return '[ ... ]';
      }

      if (value instanceof Error) {
        return JSON.stringify(value);
      }

      return value;
    })}`;

    return `[${level}] [${timestamp}]: ${message} ${dataText}`;
  });
}

export function prettyPrintFormat(withData: boolean) {
  return combine(
    format((info) => {
      info.level = info.level.toUpperCase();
      return info;
    })(),
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    format.colorize({ all: true }),
    developmentPrint(withData),
    format.splat(),
  );
}
