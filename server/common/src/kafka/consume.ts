import { KafkaGroupKind } from '@common/kafka/constants';
import { SchemaRegistry } from '@kafkajs/confluent-schema-registry';
import { ConsumerRegistry } from '@common/kafka/registry';
import { JobDataEvent, JobDataLocator } from '@common/job/event';
import { kafkaMessageToEvent } from '@common/kafka/map';

type ConsumeOptions = {
  consumerKey: string;
  groupId: KafkaGroupKind;
};

export async function consumeDataEventsForLocator(
  locator: JobDataLocator,
  schemaRegistry: SchemaRegistry,
  consumerRegistry: ConsumerRegistry,
  options: ConsumeOptions,
) {
  const consumer = consumerRegistry.getConsumer(options.consumerKey, { groupId: options.groupId });

  await consumer.connect();
  await consumer.subscribe({ topic: locator.topic, fromBeginning: false });

  return new ReadableStream<JobDataEvent>({
    start(controller) {
      consumer.run({
        eachMessage: async ({ message }) => {
          try {
            if (BigInt(message.offset) > locator.endOffset) {
              await consumer.stop();
              return;
            }

            if (BigInt(message.offset) < locator.startOffset) {
              return;
            }

            const event = await kafkaMessageToEvent(message, schemaRegistry);
            controller.enqueue(event as JobDataEvent);
          } finally {
            controller.close();
            await consumer.disconnect();
          }
        },
      });
    },
  });
}
