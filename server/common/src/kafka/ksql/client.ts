import { KsqlClientError } from "@common/kafka/ksql/error";
import { KsqlStream } from "@common/kafka/ksql/stream";
import { mustExist } from "@common/utils/exists";
import { AnyObject } from "@common/utils/types";

type KsqlQueryHeader = {
  queryId: string;
  columnNames: Array<string>;
  columnTypes: Array<string>;
};

type KsqlQueryProps = {
  sql: string;
  properties?: AnyObject;
  sessionVariables?: AnyObject;
}

export type KsqlQueryResponse = [KsqlQueryHeader, ...Array<any>];

const headers = {
  'Accept': 'application/vnd.ksqlapi.delimited.v1',
  'Content-Type': 'application/vnd.ksqlapi.delimited.v1',
};

export class KsqlClient {
  public constructor(private readonly host: string) {}

  public async makeQuery<T extends AnyObject>(props: KsqlQueryProps): Promise<KsqlStream<T>> {
    const response = await fetch(`${this.host}/query-stream`, {
      method: 'POST',
      body: JSON.stringify(props),
      headers,
    });

    if (!response.ok) {
      throw new KsqlClientError('Failed to fetch data with KSQL', { response });
    }

    const reader = mustExist(response.body).getReader();
    return new KsqlStream(reader);
  }
}
