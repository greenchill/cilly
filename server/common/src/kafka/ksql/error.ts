import { DataError } from "@common/utils/error";

export class KsqlClientError extends DataError {}

export class KsqlStreamError extends DataError {}

export class TransformStreamError extends DataError {}
