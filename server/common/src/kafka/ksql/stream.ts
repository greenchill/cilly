import { KsqlClientError, TransformStreamError } from "@common/kafka/ksql/error";

export class KsqlStream<T> extends ReadableStream<T> {
  constructor(reader: ReadableStreamDefaultReader<Uint8Array>) {
    let buffer = '';

    const pull = async (controller: ReadableStreamDefaultController<T>) => {
      try {
        while (true) {
          const { value, done } = await reader.read();
          if (done) {
            controller.close();
            break;
          }

          buffer += new TextDecoder().decode(value, { stream: true });

          let boundary;
          while ((boundary = buffer.indexOf('\n')) !== -1) {
            const part = buffer.slice(0, boundary);
            buffer = buffer.slice(boundary + 1);

            if (part) {
              try {
                const parsedData = JSON.parse(part);
                controller.enqueue(parsedData);
              } catch (error) {
                throw new KsqlClientError('Failed to parse stream data', { error });
              }
            }
          }
        }
      } finally {
        reader.releaseLock();
      }
    };

    super({ pull });
  }

  public async *[Symbol.asyncIterator]() {
    const reader = this.getReader();

    try {
      while (true) {
        const { value, done } = await reader.read();
        if (done) {
          break;
        }
        yield value;
      }
    } finally {
      reader.releaseLock();
    }
  }
}

export function createTransformStream<TInput, TOutput>(transformFunction: (input: TInput) => TOutput) {
  return new TransformStream<TInput, TOutput>({
    transform(chunk, controller) {
      try {
        const transformedData = transformFunction(chunk);
        controller.enqueue(transformedData);
      } catch (error) {
        throw new TransformStreamError('Failed to transform data', { error });
      }
    }
  });
}

export async function* streamIterator<TValue>(stream: ReadableStream<TValue> | TransformStream<any, TValue>): AsyncGenerator<TValue, void, undefined> {
  let reader: ReadableStreamDefaultReader;
  if (stream instanceof ReadableStream) {
    reader = stream.getReader();
  } else {
    reader = stream.readable.getReader();
  }
  
  try {
    while (true) {
      const { done, value } = await reader.read();
      if (done) break;
      yield value;
    }
  } finally {
    reader.releaseLock();
  }
}
