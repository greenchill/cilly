export const KafkaGroup = {
  GraphState: 'graph-state',
  Jobs: 'jobs',
} as const;

export type KafkaGroupKind = keyof typeof KafkaGroup;

export const KafkaTopic = {
  GraphState: 'graph-state-topic',
  Jobs: 'jobs-topic',
} as const;

export type KafkaTopicKind = keyof typeof KafkaTopic;
