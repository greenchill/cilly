import { Consumer, ConsumerConfig, Kafka, Producer } from 'kafkajs';

export class ProducerRegistry {
  private producers = new Map<string, Producer>();

  public constructor(private readonly client: Kafka) {}

  public getProducer(key: string): Producer {
    if (this.producers.has(key)) {
      const producer = this.client.producer();
      this.producers.set(key, producer);
      return producer;
    }

    return this.producers.get(key)!;
  }

  public async disconnectAll() {
    for (const producer of this.producers.values()) {
      await producer.disconnect();
    }

    this.producers.clear();
  }
}

export class ConsumerRegistry {
  private consumers = new Map<string, Consumer>();

  public constructor(private readonly client: Kafka) {}

  public getConsumer(key: string, config: ConsumerConfig): Consumer {
    if (!this.consumers.has(key)) {
      const consumer = this.client.consumer(config);
      this.consumers.set(key, consumer);
      return consumer;
    }

    return this.consumers.get(key)!;
  }

  public async disconnectAll() {
    for (const consumer of this.consumers.values()) {
      await consumer.disconnect();
    }

    this.consumers.clear();
  }
}

