import { SchemaRegistry } from '@kafkajs/confluent-schema-registry';
import { getKafkaEventMetadata } from '@common/events/decorator';
import { SerializableEvent } from '@common/events/serializable';
import { exists } from '@common/utils/exists';
import { KafkaMessage, Message } from 'kafkajs';
import { parseISO } from 'date-fns';
import { DataError } from '@common/utils/error';

export class KafkaMessageError extends DataError {}

export async function kafkaMessageToEvent<TEvent extends SerializableEvent = SerializableEvent>(
  { timestamp, value, offset }: KafkaMessage,
  registry: SchemaRegistry,
): Promise<TEvent | null> {
  if (!exists(value)) {
    throw new KafkaMessageError('Cannot convert message to event - value is null', {
      timestamp,
      offset,
    });
  }

  const { id, type, data }: SerializableEvent = await registry.decode(value);
  const eventTimestamp = parseISO(timestamp);
  const { constructor: EventConstructor } = getKafkaEventMetadata(type);
  return new EventConstructor(data, id, eventTimestamp) as TEvent;
}

export async function eventToKafkaMessage<TEvent extends SerializableEvent>(
  event: TEvent,
  registry: SchemaRegistry,
): Promise<Message> {
  const { subject } = getKafkaEventMetadata(event.constructor.name);
  const schemaId = await registry.getLatestSchemaId(subject);
  const value = await registry.encode(schemaId, event.serialize());

  return { value };
}
