import avro from 'avsc';

export class DateLogicalType extends avro.types.LogicalType {
  _fromValue(val: string) {
    return new Date(val);
  }

  _toValue(date: Date): number {
    return date.getTime();
  }

  _resolve(type: any) {
    if (avro.Type.isType(type, 'long', 'string', 'logical:timestamp-millis')) {
      return this._fromValue;
    }
  }
}

export class BigIntLogicalType extends avro.types.LogicalType {
  _fromValue(val: string) {
    return BigInt(val);
  }

  _toValue(bigValue: bigint): string {
    return bigValue.toString();
  }

  _resolve(type: any) {
    if (avro.Type.isType(type, 'string', 'logical:bigint')) {
      return this._fromValue;
    }
  }
}
