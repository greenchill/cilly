export type Nil = null | undefined;
export type Optional<T> = T | Nil;

export function isNil<T>(value: Optional<T>): value is Nil {
  if (value === null || value === undefined) {
    return true;
  }

  return false;
}

export function exists<T>(value: Optional<T>): value is T {
  return !isNil(value);
}

export async function existsAsync<T>(resolver: () => Promise<Optional<T>>) {
  const result = await resolver();
  return exists(result);
}

export function mustExist<T>(value: Optional<T>): T {
  if (isNil(value)) {
    throw new Error('Value must exist!');
  }

  return value;
}

export function existsBulk(values: Array<any>) {
  for (const value of values) {
    if (isNil(value)) {
      return false;
    }
  }

  return true;
}

export function existsAny(...values: Array<any>) {
  for (const value of values) {
    if (!isNil(value)) {
      return true;
    }
  }

  return false;
}

export function countExisting(...values: Array<any>) {
  return values.reduce<number>((result, value) => {
    if (exists(value)) {
      return result + 1;
    }

    return result;
  }, 0);
}
