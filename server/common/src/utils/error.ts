import { AnyObject } from "@common/utils/types";

export class DataError extends Error {
  public constructor(message?: string, public readonly data?: AnyObject) {
    super(message);
  }
}

