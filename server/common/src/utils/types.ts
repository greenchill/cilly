export type AnyObject = Record<string, any>;
export type EmptyObject = Record<string, never>;

export type Newable<T> = { new (...args: any[]): T };
