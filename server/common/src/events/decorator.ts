import { SerializableEvent } from '@common/events/serializable';
import { Newable } from '@common/utils/types';
import { DataError } from '@common/utils/error';
import { exists } from '@common/utils/exists';
import 'reflect-metadata';

const KAFKA_EVENT_METADATA_KEY = 'KAFKA_EVENT_METADATA';

type KafkaEventMetadataEntry = {
  constructor: Newable<SerializableEvent>;
  subject: string;
};

type KafkaEventMetadata = Record<string, KafkaEventMetadataEntry>;

class KafkaEventMetadataError extends DataError {}

export function KafkaEvent(subject: string) {
  return (target: Newable<SerializableEvent>) => {
    const existingMetadata: KafkaEventMetadata =
      Reflect.get(Reflect, KAFKA_EVENT_METADATA_KEY) || {};
    if (exists(existingMetadata[target.constructor.name])) {
      throw new KafkaEventMetadataError(`Metadata for ${target.constructor.name} already exists!`);
    }

    const nextMetadata: KafkaEventMetadata = {
      ...existingMetadata,
      [target.constructor.name]: {
        constructor: target.constructor as Newable<SerializableEvent>,
        subject,
      },
    };

    Reflect.set(Reflect, KAFKA_EVENT_METADATA_KEY, nextMetadata);
  };
}

export function getKafkaEventMetadata(constructorName: string): KafkaEventMetadataEntry {
  const metadata = Reflect.get(Reflect, KAFKA_EVENT_METADATA_KEY) || {};
  if (!exists(metadata)) {
    throw new KafkaEventMetadataError(
      `Metadata for ${constructorName} does not exist! Did you forget a decorator?`,
    );
  }

  return metadata[constructorName];
}
