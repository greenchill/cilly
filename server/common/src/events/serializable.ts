import { AnyObject } from '@common/utils/types';
import * as uuid from 'uuid';

export interface SerializedEvent<TEventData extends AnyObject = AnyObject> {
  id: string;
  type: string;
  timestamp: Date;
  data: TEventData;
}

export type SerializableEventConstructor<T extends AnyObject = AnyObject> = { new (data: AnyObject, id?: string, timestamp?: Date): SerializedEvent<T> };

export abstract class SerializableEvent<TEventData extends AnyObject = AnyObject> {
  public get type() {
    return this.constructor.name;
  }

  public constructor(
    public readonly data: TEventData,
    public readonly id = uuid.v4(),
    public readonly timestamp = new Date(),
  ) {}

  public serialize(): SerializedEvent<TEventData> {
    return {
      id: this.id,
      type: this.type,
      timestamp: this.timestamp,
      data: this.data,
    } 
  }
}
