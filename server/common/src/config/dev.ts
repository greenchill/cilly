import { mustExist } from '@common/utils/exists';

export const devConfig = {
  isDevelopment: mustExist(process.env.NODE_ENV) !== 'production',
  isProduction: mustExist(process.env.NODE_ENV) === 'production',
};
