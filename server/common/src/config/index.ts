import { dbConfig } from '@common/config/db';
import { devConfig } from '@common/config/dev';
import { kafkaConfig } from '@common/config/kafka';

export const coreConfig = {
  ...devConfig,
  db: dbConfig,
  kafka: kafkaConfig,
};
