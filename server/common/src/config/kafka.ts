import { mustExist } from '@common/utils/exists';

function parseKafkaBrokers() {
  const brokerData = mustExist(process.env.KAFKA_BROKERS);
  const brokers = brokerData.split(',');

  if (!Array.isArray(brokers)) {
    throw new Error('Failed to parse kafka configuration.');
  }

  return brokers;
}

export const kafkaConfig = {
  brokers: parseKafkaBrokers(),
  ksql: {
    host: mustExist(process.env.KSQL_HOST),
  },
  schemaRegistry: {
    host: mustExist(process.env.KAFKA_SCHEMA_REGISTRY_HOST),
  },
};
