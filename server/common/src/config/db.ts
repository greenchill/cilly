import { mustExist } from '@common/utils/exists';

export const dbConfig = {
  name: mustExist(process.env.POSTGRES_DB),
  host: mustExist(process.env.POSTGRES_HOST),
  port: parseInt(mustExist(process.env.POSTGRES_PORT), 10),
  user: mustExist(process.env.POSTGRES_USER),
  password: mustExist(process.env.POSTGRES_PASSWORD),
};
