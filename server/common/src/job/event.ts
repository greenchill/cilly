import { KafkaEvent } from '@common/events/decorator';
import { SerializableEvent } from '@common/events/serializable';

export type JobDataLocator = {
  topic: string;
  partition: string;
  startOffset: bigint;
  endOffset: bigint;
};

export type JobCreatedData = {
  jobId: string;
  graphId: string;
  runId: string;
  nodeId: string;
  dataLocators?: Array<JobDataLocator>;
};

export type JobData = {
  jobId: string;
  data: string;
};

@KafkaEvent('jobs.job-created')
export class JobCreatedEvent extends SerializableEvent<JobCreatedData> {}

@KafkaEvent('job-data.data')
export class JobDataEvent extends SerializableEvent<JobData> {}
